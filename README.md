# SkyEng

Тестовое задание

## Frameworks and Utilites

- Cocoapods - менеджер зависимостей
- Swiftlint - проверка code style
- Generamba - генератор модулей

- RxSwift/RxCocoa - для реактивного интерфейса, хотелось бы всё приложение сделать реактивным, но пока нет такого большого опыта в RxSwift. (Начинал с друго фреймворка)
- Alamofire - для работы с сетью
- Kingfisher - для упрощения отображения картинок
- SwiftyMocky - для автогенерации моков 

## Usage
- Для Build нужно установить pods ```pod install``` в текущей директории. 
- Запустить ```Dictionary.xcworkspace```