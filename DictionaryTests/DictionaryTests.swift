//
//  DictionaryTests.swift
//  DictionaryTests
//
//  Created by Алексей Серёжин on 19.08.2020.
//  Copyright © 2020 se. All rights reserved.
//

import XCTest
import SwiftyMocky
@testable import Dictionary

class DictionaryTests: XCTestCase {

    // MARK: - Properties

    private var presenter: DetailModulePresenter?
    private var view: DetailModuleViewMock?
    private var model: DetailModuleModelMock?

    // MARK: - XCTestCase

    override func setUp() {
        super.setUp()
        self.view = DetailModuleViewMock()
        let model = DetailModuleModelMock()
        self.model = model
        let router = DetailModuleRouterMock()
        self.presenter = DetailPresenter(view: self.view, router: router, moduleOutput: nil, model: model)
    }

    override func tearDown() {
        super.tearDown()
        self.presenter = nil
        self.view = nil
    }

    // MARK: - Main tests

    func testSuccesGetWord() {
        // Given

        guard let view = self.view else {
            XCTAssertNotNil(self.view, "View is nil")
            return
        }
        guard let model = self.model else {
            XCTAssertNotNil(self.model, "Model is nil")
            return
        }

        let viewModel = DetailViewModel(text: "Text", translation: "Translation", imageUrl: nil)
        Given(model, .getWord(willReturn: viewModel))

        // When

        self.presenter?.viewDidLoad()

        // Then

        Verify(view, 1, .show(model: .any))
    }

}
