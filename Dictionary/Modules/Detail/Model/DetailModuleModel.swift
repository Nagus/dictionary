//
//  DetailModuleModel.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 09.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

//sourcery: AutoMockable
protocol DetailModuleModel {
    func getWord() -> DetailViewModel
}
