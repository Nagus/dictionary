//
//  DetailWord.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 09.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

struct DetailWord {
    let id: Int
    let text: String
    let meaning: SearchMeaning

    init?(word: Word?, meaningIndex: Int) {
        guard let word = word, let meaning = word.meanings[safe: meaningIndex] else {
            return nil
        }
        self.id = word.id
        self.text = word.text
        self.meaning = meaning
    }
}
