//
//  DetailModel.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 08.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

class DetailModel: DetailModuleModel {

    // MARK: - Private variables

    private var word: DetailWord

    init(word: DetailWord) {
        self.word = word
    }

    // MARK: - DetailModuleModel

    func getWord() -> DetailViewModel {
        let urlString = "https:" + self.word.meaning.imageUrl
        let viewModel = DetailViewModel(
            text: self.word.text,
            translation: self.word.meaning.translation.text,
            imageUrl: try? urlString.asURL()
        )
        return viewModel
    }

}
