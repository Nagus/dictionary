//
//  DetailRouter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class DetailRouter {

    // MARK: - Properties

    weak var transit: ModuleTransition?

    init(transit: ModuleTransition?) {
        self.transit = transit
    }
}

// MARK: - DetailModuleRouter

extension DetailRouter: DetailModuleRouter { }
