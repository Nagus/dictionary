//
//  DetailModuleRouter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import Foundation

//sourcery: AutoMockable
protocol DetailModuleRouter {
    init(transit: ModuleTransition?)
}
