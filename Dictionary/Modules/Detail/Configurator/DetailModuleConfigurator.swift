//
//  DetailModuleConfigurator.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class DetailModuleConfigurator {

    // MARK: - Internal methods

    func configure(word: DetailWord, output: DetailModuleOutput? = nil) -> ModuleTransition {
        let name = String(describing: DetailViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)

        guard let view = storyboard.instantiateInitialViewController() as? DetailViewController else {
            fatalError("Can't load \(name) from storyboard, check that controller is initial view controller")
        }

        let router = DetailRouter(transit: view)
        let model = DetailModel(word: word)
        let presenter = DetailPresenter(view: view, router: router, moduleOutput: output, model: model)

        view.output = presenter

        return view
    }
}
