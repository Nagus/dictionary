//
//  DetailPresenter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

final class DetailPresenter {

    // MARK: - Properties

    weak var view: DetailModuleView?
    var router: DetailModuleRouter?
    var output: DetailModuleOutput?
    let model: DetailModuleModel

    // MARK: Initialization
    init(
        view: DetailModuleView?,
        router: DetailModuleRouter?,
        moduleOutput: DetailModuleOutput?,
        model: DetailModuleModel
    ) {
        self.view = view
        self.router = router
        self.output = moduleOutput
        self.model = model
    }
}

// MARK: - DetailModulePresenter

extension DetailPresenter: DetailModulePresenter {
    func viewDidLoad() {
        let word = self.model.getWord()
        self.view?.show(model: word)
    }
}

// MARK: - DetailModuleInput

extension DetailPresenter: DetailModuleInput { }
