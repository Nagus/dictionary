//
//  DetailViewController.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit
import Kingfisher

final class DetailViewController: ViewController {

    // MARK: - IBOutlet

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var translationLabel: UILabel!

    // MARK: - Properties

    var output: DetailModulePresenter?

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        self.output?.viewDidLoad()
    }
}

// MARK: - DetailModuleView

extension DetailViewController: DetailModuleView {

    func show(model: DetailViewModel) {
        self.wordLabel.text = model.text
        self.translationLabel.text = model.translation
        self.imageView.kf.setImage(with: model.imageUrl)
    }

}
