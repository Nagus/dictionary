//
//  SearchRouter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class SearchRouter {

    // MARK: - Properties

    weak var transit: ModuleTransition?

    init(transit: ModuleTransition?) {
        self.transit = transit
    }
}

// MARK: - SearchModuleRouter

extension SearchRouter: SearchModuleRouter {
    func openDetail(word: DetailWord) {
        let module = DetailModuleConfigurator().configure(word: word)
        self.transit?.perform(module: module, segue: ShowModuleSegue(), animate: false, completion: nil)
    }
}
