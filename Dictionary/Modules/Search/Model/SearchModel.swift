//
//  SearchModel.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 06.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation
import Alamofire

class SearchModel: SearchModuleModel {

    // MARK: - Private variables

    private let service: SearchService
    private var currentPage: Int = 0
    private let pageSize: Int = 20
    private var words: [Word] = []
    private var currentWord: String = ""
    private var currentRequest: DataRequest?

    // MARK: - Inititalization

    init(service: SearchService) {
        self.service = service
    }

    // MARK: - SearchModuleModel

    func detailWord(for index: Int, meaningIndex: Int) -> DetailWord? {
        return DetailWord(word: self.words[safe: index], meaningIndex: meaningIndex)
    }

    func search(word: String, completion: @escaping ResultCompletion<[Word]>) {
        self.currentRequest?.cancel()
        self.currentPage = 0
        self.currentWord = word
        self.words = []

        if word.isEmpty {
            completion(.success(self.words))
            return
        }

        self.currentRequest = self.service
            .search(word: word, page: self.currentPage, pageSize: self.pageSize) { [weak self] result in
                self?.currentRequest = nil
                switch result {
                case .success(let words):
                    self?.words =  words
                    completion(.success(words))
                case .error(let error):
                    completion(.error(error))
                }
            }
    }

    func nextPage(completion: @escaping ResultCompletion<[Word]>) {
        guard !self.currentWord.isEmpty, self.currentRequest == nil else {
            return
        }

        self.currentPage += 1
        self.currentRequest = self.service
            .search(word: self.currentWord, page: self.currentPage, pageSize: self.pageSize) { [weak self] result in
                guard let self = self else {
                    return
                }
                self.currentRequest = nil
                switch result {
                case .success(let words):
                    if words.isEmpty {
                        self.currentPage -= 1
                    } else {
                        self.append(words: words)
                    }
                    completion(.success(self.words))
                case .error(let error):
                    completion(.error(error))
                }
            }
    }

}

// MARK: - Provate methods

extension SearchModel {

    func append(words: [Word]) {
        let newWords: [Word] = words.compactMap { word in
            let contains = self.words.contains { $0.id == word.id }
            return contains ? nil : word
        }
        self.words.append(contentsOf: newWords)
    }

}
