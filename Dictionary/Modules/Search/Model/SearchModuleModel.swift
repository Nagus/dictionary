//
//  SearchModuleModel.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

protocol SearchModuleModel {
    func detailWord(for index: Int, meaningIndex: Int) -> DetailWord?
    func search(word: String, completion: @escaping ResultCompletion<[Word]>)
    func nextPage(completion: @escaping ResultCompletion<[Word]>)
}
