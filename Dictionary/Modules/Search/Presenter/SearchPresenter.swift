//
//  SearchPresenter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

final class SearchPresenter {

    private let model: SearchModuleModel

    // MARK: - Properties

    weak var view: SearchModuleView?
    var router: SearchModuleRouter?
    var output: SearchModuleOutput?

    // MARK: - initialization

    init(
        view: SearchModuleView?,
        router: SearchModuleRouter?,
        moduleOutput: SearchModuleOutput?,
        model: SearchModuleModel
    ) {
        self.view = view
        self.router = router
        self.output = moduleOutput
        self.model = model
    }
}

// MARK: - SearchModulePresenter

extension SearchPresenter: SearchModulePresenter {

    func viewDidLoad() { }

    func willShowLast() {
        self.model.nextPage { [weak self] result in
            switch result {
            case .success(let models):
                self?.view?.show(models: models)
            case .error(let error):
                self?.view?.show(error: error)
            }
        }
    }

    func search(word: String) {
        self.model.search(word: word) { [weak self] result in
            switch result {
            case .success(let models):
                self?.view?.show(models: models)
            case .error(let error):
            self?.view?.show(error: error)
            }
        }
    }

    func didSelectWord(index: Int, meaningIndex: Int) {
        guard let word = self.model.detailWord(for: index, meaningIndex: meaningIndex) else {
            return
        }

        self.router?.openDetail(word: word)
    }

}

// MARK: - SearchModuleInput

extension SearchPresenter: SearchModuleInput { }
