//
//  SearchModulePresenter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

protocol SearchModulePresenter {
    func viewDidLoad()
    func search(word: String)
    func didSelectWord(index: Int, meaningIndex: Int)
    func willShowLast()
}
