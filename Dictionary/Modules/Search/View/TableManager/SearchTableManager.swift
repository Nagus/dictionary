//
//  SearchTableManager.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

protocol SearchTableManager {

    var didSelectWord: ((Int, Int) -> Void)? { get set }
    var willShowLast: EmptyClosure? { get set }

    func configure(tableView: UITableView)
    func update(models: [Word])

}
