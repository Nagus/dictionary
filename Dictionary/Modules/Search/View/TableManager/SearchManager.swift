//
//  SearchManager.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchManager: NSObject, SearchTableManager {

    // MARK: - Private variables

    private var tableView: UITableView?
    private var models: [Word] = []

    // MARK: - SearchTableManager

    var didSelectWord: ((Int, Int) -> Void)?
    var willShowLast: EmptyClosure?

    func configure(tableView: UITableView) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
    }

    func update(models: [Word]) {
        self.models = models
        self.tableView?.reloadData()
    }

}

// MARK: - UITableViewDelegate

extension SearchManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.models.count - 1 == indexPath.row {
            self.willShowLast?()
        }

        guard let cell = cell as? MeaningCell, let model = self.models[safe: indexPath.row] else {
            return
        }
        cell.configure(word: model)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectWord?(indexPath.row, 0)
    }
}

// MARK: - UITableViewDataSource

extension SearchManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.models.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: MeaningCell.typeName, for: indexPath)
    }
}
