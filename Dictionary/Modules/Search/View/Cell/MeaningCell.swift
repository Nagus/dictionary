//
//  MeaningCell.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

class MeaningCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var translationLabel: UILabel!

    // MARK: - Public methods

    func configure(word: Word) {
        self.titleLabel.text = word.text
        self.translationLabel.text = word.meanings.map({ $0.translation.text }).joined(separator: ", ")
    }

}
