//
//  SearchViewController.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class SearchViewController: ViewController, KeyboardScrolled {

    private enum State {
        case error
        case empty
        case fill(words: [Word])
        case notFound
    }

    // MARK: - IBOutlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var placeholderTitleLabel: UILabel!

    // MARK: Private variables

    private lazy var searchBar = UISearchBar()
    private lazy var bag = DisposeBag()
    private var manager: SearchTableManager?
    private var state: State = .empty {
        didSet {
            self.updateViewForState()
        }
    }

    // MARK: - Properties

    var output: SearchModulePresenter?

    // MARK: - KeyboardScrolled

    var showNotification: NSObjectProtocol?
    var hideNotification: NSObjectProtocol?
    var scrollView: UIScrollView {
        return self.tableView
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        self.registerForKeyboardNotifications()
    }

    deinit {
        self.unregisterForKeyboardNotifications()
    }

    // MARK: - Public methods

    func configure(manager: SearchTableManager) {
        self.manager = manager
    }

}

// MARK: - SearchModuleView

extension SearchViewController: SearchModuleView {

    func show(models: [Word]) {
        if models.isEmpty {
            if self.searchBar.text?.isEmpty ?? true {
                self.state = .empty
            } else {
                self.state = .notFound
            }
        } else {
            self.state = .fill(words: models)
        }
    }

    func show(error: Error?) {
        self.state = .error
    }

}

// MARK: - Private method

private extension SearchViewController {

    func configure() {
        self.configureNavigationBar()
        self.configureTableView()
        self.state = .empty
    }

    func configureNavigationBar() {
        self.navigationItem.titleView = self.searchBar
        self.searchBar.placeholder = "Search new word"
        self.searchBar.rx.text
            .orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { [weak self] result in
                switch result {
                case .next(let text):
                    self?.output?.search(word: text)
                default:
                    return
                }
            }
            .disposed(by: self.bag)
    }

    func configureTableView() {
        self.manager?.configure(tableView: self.tableView)
        self.manager?.didSelectWord = { [weak self] index, meaningIndex in
            self?.output?.didSelectWord(index: index, meaningIndex: meaningIndex)
        }
        self.manager?.willShowLast = { [weak self] in
            self?.output?.willShowLast()
        }
    }

    func updateViewForState() {
        switch self.state {
        case .error:
            self.showPlaseholderView(with: "Something went wrong 😵")
        case .empty:
            self.showPlaseholderView(with: "📖 Look up the words")
        case .notFound:
            self.showPlaseholderView(with: "Nothing found")
        case.fill(let words):
            self.placeholderView.isHidden = true
            self.manager?.update(models: words)
        }

    }

    func showPlaseholderView(with text: String) {
        self.manager?.update(models: [])
        self.placeholderTitleLabel.text = text
        self.placeholderView.isHidden = false
    }

}
