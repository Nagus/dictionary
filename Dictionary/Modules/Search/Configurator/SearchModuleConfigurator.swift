//
//  SearchModuleConfigurator.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class SearchModuleConfigurator {

    // MARK: - Internal methods

    func configure(output: SearchModuleOutput? = nil) -> ModuleTransition {
        let name = String(describing: SearchViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)

        guard let view = storyboard.instantiateInitialViewController() as? SearchViewController else {
            fatalError("Can't load \(name) from storyboard, check that controller is initial view controller")
        }

        let manager = SearchManager()
        let router = SearchRouter(transit: view)
        let service = SearchService()
        let model = SearchModel(service: service)
        let presenter = SearchPresenter(view: view, router: router, moduleOutput: output, model: model)

        view.configure(manager: manager)
        view.output = presenter

        return view
    }
}
