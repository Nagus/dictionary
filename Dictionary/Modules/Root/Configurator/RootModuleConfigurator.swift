//
//  RootModuleConfigurator.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class RootModuleConfigurator {

    // MARK: - Internal methods

    func configure(output: RootModuleOutput? = nil) -> ModuleTransition {
        let name = String(describing: RootViewController.self)
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)

        guard let view = storyboard.instantiateInitialViewController() as? RootViewController else {
            fatalError("Can't load \(name) from storyboard, check that controller is initial view controller")
        }

        let router = RootRouter(transit: view)
        let presenter = RootPresenter(view: view, router: router, moduleOutput: output)

        view.output = presenter

        return view
    }
}
