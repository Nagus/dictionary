//
//  RootRouter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class RootRouter {

    // MARK: - Properties

    weak var transit: ModuleTransition?

    init(transit: ModuleTransition?) {
        self.transit = transit
    }
}

// MARK: - RootModuleRouter

extension RootRouter: RootModuleRouter {
    func openSearching() {
        let module = SearchModuleConfigurator().configure()
        self.transit?.perform(module: module, segue: SetRootModuleSegue(), animate: false, completion: nil)
    }
}
