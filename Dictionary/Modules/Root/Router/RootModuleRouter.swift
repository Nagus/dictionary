//
//  RootModuleRouter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import Foundation

protocol RootModuleRouter {

    func openSearching()

    init(transit: ModuleTransition?)
}
