//
//  RootPresenter.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

final class RootPresenter {

    // MARK: - Properties

    weak var view: RootModuleView?
    var router: RootModuleRouter?
    var output: RootModuleOutput?

    init(view: RootModuleView?,
         router: RootModuleRouter?,
         moduleOutput: RootModuleOutput?
    ) {
        self.view = view
        self.router = router
        self.output = moduleOutput
    }
}

// MARK: - RootModulePresenter

extension RootPresenter: RootModulePresenter {
    func viewDidLoad() {
        self.router?.openSearching()
    }
}

// MARK: - RootModuleInput

extension RootPresenter: RootModuleInput { }
