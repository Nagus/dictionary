//
//  RootViewController.swift
//  Dictionary
//
//  Created by Aleksey Serezhin on 07/09/2020.
//  Copyright © 2020 SkyEng. All rights reserved.
//

import UIKit

final class RootViewController: UINavigationController, ModuleTransition {

    // MARK: - ModuleTransition

    var showedSegue: Segue?

    // MARK: - Properties

    var output: RootModulePresenter?

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        self.output?.viewDidLoad()
    }
}

// MARK: - RootModuleView

extension RootViewController: RootModuleView { }
