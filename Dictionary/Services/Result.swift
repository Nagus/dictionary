//
//  Result.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 06.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error?)
}
