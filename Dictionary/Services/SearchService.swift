//
//  SearchService.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 03.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation
import Alamofire

class SearchService {

    // MARK: - Private variables

    private let session: Session

    // MARK: - Initialization

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 // seconds
        self.session = Session(configuration: configuration)
    }

    @discardableResult
    func search(
        word: String,
        page: Int? = nil,
        pageSize: Int? = nil,
        completion: @escaping ResultCompletion<[Word]>
    ) -> DataRequest {
        let params = SearchParametres(search: word, page: page, pageSize: pageSize)
        return self.getRequest(endpoint: Endponts.search, params: params, completion: completion)
    }

    @discardableResult
    func getRequest<T: Decodable, Parametres: Encodable>(
        endpoint: String,
        params: Parametres?,
        completion: @escaping ResultCompletion<T>
    ) -> DataRequest {
        return self.session
            .request(endpoint, method: .get, parameters: params, encoder: URLEncodedFormParameterEncoder.default)
            .responseDecodable { (response: DataResponse<T, AFError>) in
                if case .explicitlyCancelled = response.error {
                    return
                }
                let result: Result<T>

                if let error = response.error {
                    result = .error(error)
                } else if let value = response.value {
                    result = .success(value)
                } else {
                    result = .error(nil)
                }
                completion(result)
            }
    }

}
