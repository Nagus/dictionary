//
//  AppDelegate.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 19.08.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // MARK: - ModuleTransition

    var showedSegue: Segue?

    // MARK: - UIApplicationDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.window = self.makeWindow()
        let rootModule = RootModuleConfigurator().configure()
        self.perform(module: rootModule, segue: SetRootModuleSegue(), animate: false, completion: nil)
        return true
    }

}

// MARK: - ModuleTransition

extension AppDelegate: ModuleTransition { }

// MARK: - Private methods

private extension AppDelegate {
    func makeWindow() -> UIWindow {
        let window = UIWindow()
        window.makeKeyAndVisible()
        return window
    }
}
