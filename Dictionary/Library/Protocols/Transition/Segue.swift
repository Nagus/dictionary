//
//  Segue.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

protocol Segue {
    func go(from source: ModuleTransition,
            to destination: ModuleTransition,
            animate: Bool,
            completion: EmptyClosure?)
    func goBack(animate: Bool, completion: EmptyClosure?)
}
