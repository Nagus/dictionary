//
//  ModuleTransition.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

protocol ModuleTransition: class {
    var showedSegue: Segue? { get set }

    func perform(module: ModuleTransition, segue: Segue, animate: Bool, completion: EmptyClosure?)
    func close(animate: Bool, completion: EmptyClosure?)
}

// MARK: - Default implementation

extension ModuleTransition {
    func perform(module: ModuleTransition, segue: Segue, animate: Bool, completion: EmptyClosure?) {

        module.showedSegue = segue
        segue.go(from: self, to: module, animate: animate, completion: completion)
    }

    func close(animate: Bool, completion: EmptyClosure?) {
        self.showedSegue?.goBack(animate: animate, completion: completion)
    }
}
