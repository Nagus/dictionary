//
//  TypeDescribing.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

protocol TypeDescribing {
    static var typeName: String { get }

}

extension TypeDescribing {
    static var typeName: String {
        String(describing: Self.self)
    }
}
