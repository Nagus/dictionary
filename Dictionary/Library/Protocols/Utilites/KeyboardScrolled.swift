//
//  KeyboardScrolled.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 09.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

protocol KeyboardScrolled: class {

    var scrollView: UIScrollView { get }

    var showNotification: NSObjectProtocol? { get set }
    var hideNotification: NSObjectProtocol? { get set }

    func registerForKeyboardNotifications()
    func unregisterForKeyboardNotifications()
    func keyboardWillShow(notification: Notification)
    func keyboardWillHide(notification: Notification)
}

extension KeyboardScrolled {
    func registerForKeyboardNotifications() {
        let center = NotificationCenter.default
        self.showNotification = center.addObserver(
            forName: UIResponder.keyboardWillShowNotification,
            object: nil,
            queue: nil,
            using: self.keyboardWillShow
        )
        self.hideNotification = center.addObserver(
            forName: UIResponder.keyboardWillHideNotification,
            object: nil,
            queue: nil,
            using: self.keyboardWillHide
        )
    }

    func unregisterForKeyboardNotifications() {
        let center = NotificationCenter.default

        if let hideNotification = self.hideNotification {
            center.removeObserver(hideNotification)
        }
        if let showNotification = self.showNotification {
            center.removeObserver(showNotification)
        }
    }

    func keyboardWillShow(notification: Notification) {
        guard
            let userInfo = notification.userInfo as? [String: Any],
            let keyboardInfo = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue
        else {
            return
        }

        let keyboardHeight = keyboardInfo.cgRectValue.size.height
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }

    func keyboardWillHide(notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }

}
