//
//  SetRootModuleSegue.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

class SetRootModuleSegue: Segue {

    // MARK: - ModuleSegueProtocol

    func go(from source: ModuleTransition,
            to destination: ModuleTransition,
            animate: Bool, completion: EmptyClosure?) {

        switch (source, destination) {
        case (let source as UINavigationController, let destination as UIViewController):
            self.setRoot(from: source, to: destination, animate: animate, completion: completion)
        case (let source as UIViewController, let destination as UIViewController):
            self.setRoot(from: source.navigationController, to: destination, animate: animate, completion: completion)
        case (let source as AppDelegate, let destination as UIViewController):
            self.setRoot(from: source, to: destination, animate: animate, completion: completion)
        default:
            fatalError("Wrong segue")
        }
    }

    func goBack(animate: Bool, completion: EmptyClosure?) { }
}

// MARK: - Set root from UIViewController to UIViewControllers

private extension SetRootModuleSegue {
    func setRoot(from source: UINavigationController?,
                 to destination: UIViewController,
                 animate: Bool,
                 completion: EmptyClosure?) {

        if let navigationController = source {
            navigationController.setViewControllers([destination], animated: animate)
        } else {
            UIApplication.shared.windows.first?.rootViewController = destination
        }
        completion?()
    }

    func setRoot(from source: AppDelegate,
                 to destination: UIViewController,
                 animate: Bool,
                 completion: EmptyClosure?) {
        source.window?.rootViewController = destination
        completion?()
    }
}
