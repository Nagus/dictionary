//
//  ShowModuleSegue.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import UIKit

class ShowModuleSegue: Segue {

    // MARK: - Private propeties

    private weak var destination: ModuleTransition?

    // MARK: - ModuleSegueProtocol

    func go(from source: ModuleTransition,
            to destination: ModuleTransition,
            animate: Bool,
            completion: EmptyClosure?) {

        self.destination = destination

        switch (source, destination) {
        case (let source as UIViewController, let destination as UIViewController):
            self.show(from: source, to: destination, animate: animate, completion: completion)
        default:
            fatalError("Wrong segue")
        }

    }

    func goBack(animate: Bool, completion: EmptyClosure?) {
        switch self.destination {
        case let destination as UIViewController:
            self.goBack(from: destination, animate: animate, completion: completion)
        default:
            fatalError("Wrong segue")
        }
    }
}

// MARK: - Show from UIViewController to UIViewController

private extension ShowModuleSegue {
    func show(from source: UIViewController,
              to destination: UIViewController,
              animate: Bool,
              completion: EmptyClosure?) {

        source.navigationController?.pushViewController(destination, animated: animate)
        completion?()
    }

    func goBack(from source: UIViewController, animate: Bool, completion: EmptyClosure?) {
        source.navigationController?.popViewController(animated: animate)
        completion?()
    }
}
