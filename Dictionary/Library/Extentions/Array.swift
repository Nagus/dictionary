//
//  Array.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

extension Array {

    /// Safe subscript

    subscript(safe index: Int) -> Element? {
        guard index < self.count && index >= 0 else {
            return nil
        }

        return self[index]
    }

}
