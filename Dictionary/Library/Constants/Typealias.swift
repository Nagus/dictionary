//
//  Typealias.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 07.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

// MARK: - Functions

typealias EmptyClosure = () -> Void
typealias ResultCompletion<T> = (Result<T>) -> Void
