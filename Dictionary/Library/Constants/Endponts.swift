//
//  Endponts.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 04.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

enum Endponts {
    static let host = "https://dictionary.skyeng.ru"
    static let search = Self.host + "/api/public/v1/words/search"
    static let meanings = Self.host + "/api/public/v1/meanings"
}
