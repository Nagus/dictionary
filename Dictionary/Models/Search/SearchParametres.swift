//
//  SearchParametres.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 04.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

struct SearchParametres: Encodable {
    let search: String
    let page: Int?
    let pageSize: Int?
}
