//
//  Translation.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 03.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

struct Translation: Decodable {
    let text: String
    let note: String?
}
