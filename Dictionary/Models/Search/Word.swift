//
//  Word.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 03.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

struct Word: Decodable {
    let id: Int
    let text: String
    let meanings: [SearchMeaning]
}
