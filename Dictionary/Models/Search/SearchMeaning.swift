//
//  SearchMeaning.swift
//  Dictionary
//
//  Created by Алексей Серёжин on 03.09.2020.
//  Copyright © 2020 se. All rights reserved.
//

import Foundation

struct SearchMeaning: Decodable {
    let id: Int
    let partOfSpeechCode: String
    let translation: Translation
    let previewUrl: String
    let imageUrl: String
    let transcription: String
    let soundUrl: String
}
